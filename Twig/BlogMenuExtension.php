<?php

namespace Xaben\BlogBundle\Twig;

use Doctrine\ORM\EntityManager;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class BlogMenuExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'blog_menu',
                array($this, 'blogMenu'),
                array(
                    'is_safe' => array('html'),
                    'needs_environment' => true,
                )
            ),
            new \Twig_SimpleFunction(
                'blog_menu_calendar',
                array($this, 'blogMenuCalendar'),
                array(
                    'is_safe' => array('html'),
                    'needs_environment' => true,
                )
            ),
            new \Twig_SimpleFunction(
                'blog_menu_categories',
                array($this, 'blogMenuCategories'),
                array(
                    'is_safe' => array('html'),
                    'needs_environment' => true,
                )
            ),
        );
    }

    /**
     * @param \Twig_Environment $twig
     * @return string
     */
    public function blogMenu(\Twig_Environment $twig)
    {
        return $twig->render('@XabenBlog/Partials/menu.html.twig');
    }

    /**
     * @param \Twig_Environment $twig
     * @return string
     */
    public function blogMenuCalendar(\Twig_Environment $twig)
    {
        $calendar = $this
            ->entityManager
            ->getRepository('XabenBlogBundle:Post')
            ->getCalendar();

        return $twig->render(
            '@XabenBlog/Partials/menu_calendar.html.twig',
            array('calendar' => $calendar)
        );
    }

    /**
     * @param \Twig_Environment $twig
     * @return string
     */
    public function blogMenuCategories(\Twig_Environment $twig)
    {
        $categories = $this
            ->entityManager
            ->getRepository('XabenBlogBundle:Category')
            ->findBy([], ['title' => 'ASC']);

        return $twig->render(
            '@XabenBlog/Partials/menu_categories.html.twig',
            array('categories' => $categories)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'blog_menu';
    }
}
