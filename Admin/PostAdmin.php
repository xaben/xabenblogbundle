<?php
namespace Xaben\BlogBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Xaben\BlogBundle\Entity\Post;
use Xaben\BlogBundle\Form\Type\BlogTagSelectorType;

class PostAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('slug')
        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Post',
                array(
                    'class'       => 'col-md-8',
                ))
            ->add('title', TextType::class, array('required' => true))
            ->add('content', CKEditorType::class, array(
                'config_name' => 'blog',
            ))
            ->end()

            ->with('Info',
                array(
                    'class' => 'col-md-4'
                ))
            ->add('category', 'sonata_type_model_list', array('required' => true))
            ->add('cover', 'sonata_type_model_list', array('required'=>false), array('link_parameters' => array('context' => 'blog')))
            ->end()

            ->with('SEO',
                array(
                    'class' => 'col-md-4'
                ))
            ->add('seoTitle', TextType::class, array('required' => false, 'label' => 'Title'))
            ->add('seodesc', TextType::class, array('required' => false, 'label' => 'Description'))
            ->add('keywords', TextType::class, array('required' => false))
            ->end()

            ->with('Tags',
                array(
                    'class' => 'col-md-4'
                ))
            ->add('tags', BlogTagSelectorType::class, array('required'=>false, 'label'=>'Tags', 'attr'=> array('data-xaben'=>'blog','class'=> 'span5', 'style'=>"width: 100%")))
            ->end()

            ->with('Status',
                array(
                    'class'       => 'col-md-4',
                ))
            ->add('publishDate', DateTimePickerType::class, array(
                'required'  => false,
            ))
            ->add('status', ChoiceType::class, array(
                'choices'   => Post::getStatuses(),
                'required'  => true,
            ))
            ->end()
        ;
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('status', 'choice', array(
                'choices'   => Post::getStatuses(),
            ))
            ->add('category')
            ->add('modified')
        ;
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug')
        ;
    }
}
