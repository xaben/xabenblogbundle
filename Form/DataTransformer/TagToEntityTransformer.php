<?php

namespace Xaben\BlogBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Xaben\BlogBundle\Entity\Tag;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class TagToEntityTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (tag) to a string (tag text comma separated).
     *
     * @param  Tag|null $tagObjects
     * @return string
     */
    public function transform($tagObjects)
    {

        if (null === $tagObjects) {
            return "";
        }
        $string = '';

        /**
         * @var integer $key
         * @var Tag $tagObj
         */
        foreach($tagObjects as $key => $tagObj) {
            if ($key == 0){
                $string .= $tagObj->getTitle();
            }else{
                $string .= ';'.$tagObj->getTitle();
            }

        }

        return $string;
    }

    /**
     * Transforms a string (number) to an object (tag).
     *
     * @param  string $text
     *
     * @return Tag|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($text)
    {
        if (!$text) {
            return null;
        }
        $list = explode(';',$text);
        $tagObjs = [];

        foreach ($list as $tagText){
            $tagText = trim($tagText);
            if (!empty($tagText)){
                $tagObj = $this->om
                    ->getRepository('XabenBlogBundle:Tag')
                    ->findOneBy(array('title' => $tagText));

                if (null === $tagObj) {
                    //create a new tagObj and save it
                    $tagObj = new Tag();
                    $tagObj->setTitle($tagText);
                }

                $tagObjs[] = $tagObj;
            }
        }
        $results=new ArrayCollection($tagObjs);
        return $results;
    }
}
