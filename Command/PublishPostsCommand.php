<?php

namespace Xaben\BlogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PublishPostsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('xaben:blog:publish-posts')
            ->setDescription('Publish posts');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $updated = $this->getContainer()->get('xaben.blog.repository.post')->publishPosts();
        $output->writeln(sprintf('%d posts were published.', $updated));
    }
}
