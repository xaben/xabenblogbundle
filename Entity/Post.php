<?php

namespace Xaben\BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="blog__post")
 * @ORM\Entity(repositoryClass="Xaben\BlogBundle\Entity\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class Post
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 2;
    const STATUS_PENDING  = 3;
    const STATUS_DELETED = 4;
    const STATUS_SCHEDULED = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="posts")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"}, orphanRemoval=true, fetch="EAGER")
     */
    private $cover;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     * @Assert\Choice(callback = "getStatusesValidator")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime", nullable=true)
     */
    private $publishDate;

    /**
     * @var \DateTime $published
     *
     * @ORM\Column(name="published", type="datetime", nullable=true)
     */
    private $published;

    /**
     * @var \DateTime $modified
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="posts", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="blog__post_to_tag")
     **/
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $seoTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="seodesc", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $seodesc;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "3",
     *      max = "255"
     * )
     */
    private $keywords;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer")
     *
     */
    private $views;

    public function __toString()
    {
        return $this->getTitle() ? $this->getTitle() : '';
    }

    public function __construct()
    {
        //set status to draft by default
        $this->setStatus(2);
        $this->tags = new ArrayCollection();
        $this->views = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Post
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get array with available statuses for choice field
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            'Published' => self::STATUS_PUBLISHED,
            'Scheduled' => self::STATUS_SCHEDULED,
            'Draft' => self::STATUS_DRAFT,
            'Pending review' => self::STATUS_PENDING,
            'Deleted' => self::STATUS_DELETED,
        ];
    }

    /**
     * Get keys for validation of statuses
     *
     * @return array
     */
    public static function getStatusesValidator()
    {
        $statuses = self::getStatuses();

        return array_values($statuses);
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param \DateTime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Update published according to status
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @throws \Exception
     * @return $this
     */
    public function process()
    {
        //publish
        if (($this->getStatus() == 1) && ($this->published == null)) {
            $this->setPublished(new \DateTime());
        }

        //unpublish
        if (($this->getStatus() != 1) && ($this->published != null)) {
            $this->setPublished(null);
        }

        //get description
        $parts = explode('<!--more-->', $this->getContent());

        if (count($parts) == 2){
            $this->setDescription($parts[0]);
        } elseif (count($parts) >2) {
            throw new \Exception('Content cannot have more then 2 split parts');
        } else {
            $this->setDescription($this->getContent());
        }

        return $this;
    }


    /**
     * Set category
     *
     * @param Category $category
     * @return Post
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set tags
     *
     * @param Tag $tags
     * @return Post
     */
    public function setTags($tags)
    {
        //remove all prev tags
        $old_tags = $this->getTags();
        foreach ($old_tags as $tag){
            $this->removeTag($tag);
        }

        //add tags
        if (is_array($tags)){
            foreach ($tags as $tag){
                $this->addTag($tag);
            }
        }

        return $this;
    }

    /**
     * Add tags
     *
     * @param Tag $tags
     * @return Post
     */
    public function addTag(Tag $tags)
    {
        $tags->addPosts($this);
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param Tag $tags
     */
    public function removeTag(Tag $tags)
    {
        $tags->removePosts($this);
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set cover
     *
     * @param MediaInterface $cover
     * @return Post
     */
    public function setCover(MediaInterface $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return MediaInterface
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Post
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param string $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * Set seodesc
     *
     * @param string $seodesc
     * @return Post
     */
    public function setSeodesc($seodesc)
    {
        $this->seodesc = $seodesc;

        return $this;
    }

    /**
     * Get seodesc
     *
     * @return string 
     */
    public function getSeodesc()
    {
        return $this->seodesc;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Post
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Post
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }
}
