<?php

namespace Xaben\BlogBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Xaben\BlogBundle\Entity\Category;
use Xaben\BlogBundle\Entity\Post;
use Xaben\BlogBundle\Entity\Tag;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class PostRepository extends EntityRepository
{
    /** @var int */
    protected $itemsPerPage;

    /** @var Paginator */
    protected $paginator;

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = (int)$itemsPerPage;
    }

    /**
     * @param Paginator $paginator
     */
    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @param int $itemsPerPage
     * @return array|Post[]|Post
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLatestPosts($itemsPerPage = null)
    {
        $limit = $itemsPerPage || $this->itemsPerPage;
        $query = $this
            ->createQueryBuilder('p')
            ->select('p, c')
            ->from('XabenBlogBundle:Post', 'p')
            ->leftJoin('p.cover', 'c')
            ->andWhere('p.status = :status')
            ->setParameters([
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'DESC')
            ->setMaxResults($limit);

        if ($limit === 1) {
            return $query->getQuery()->getOneOrNullResult();
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param int $page
     * @param int $itemsPerPage
     * @return SlidingPagination
     */
    public function getLatest($page, $itemsPerPage = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p, c')
            ->leftJoin('p.cover', 'c')
            ->andWhere('p.status = :status')
            ->setParameters([
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'DESC');

        /** @var SlidingPagination $pagination */
        $pagination = $this->paginator->paginate(
            $qb->getQuery(),
            $page,
            $itemsPerPage ?? $this->itemsPerPage
        );

        return $pagination;
    }

    /**
     * @param int $year
     * @param int $month
     * @param int $page
     * @param int|null $itemsPerPage
     * @return SlidingPagination
     */
    public function getByMonth($year, $month, $page, $itemsPerPage = null)
    {
        $first = "$year-$month-01";
        $last = date('Y-m-t', strtotime($first));

        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT p,c FROM XabenBlogBundle:Post p
                              LEFT JOIN p.cover c
                              WHERE p.status = 1 AND p.published >= :first AND p.published <= :last
                              ORDER BY p.published DESC
            ")
            ->setParameter('first', $first)
            ->setParameter('last', $last);

        /** @var SlidingPagination $pagination */
        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $itemsPerPage ?? $this->itemsPerPage
        );

        return $pagination;
    }

    /**
     * @param int $id
     * @return Post
     */
    public function getPost($id)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p, t, c')
            ->leftJoin('p.tags', 't')
            ->leftJoin('p.cover', 'c')
            ->where('p.id = :id')
            ->andWhere('p.status = :status')
            ->setParameters([
                'id'=> $id,
                'status' => Post::STATUS_PUBLISHED,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Post $post
     * @return Post
     */
    public function getPreviousPost(Post $post)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.published > :published')
            ->andWhere('p.status = :status')
            ->setParameters([
                'published' => $post->getPublished(),
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'ASC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Post $post
     * @return Post
     */
    public function getNextPost(Post $post)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.published < :published')
            ->andWhere('p.status = :status')
            ->setParameters([
                'published' => $post->getPublished(),
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Category $category
     * @param int $page
     * @param int|null $itemsPerPage
     * @return SlidingPagination
     */
    public function getByCategory(Category $category, $page, $itemsPerPage = null)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p, m')
            ->leftJoin('p.cover', 'm')
            ->innerJoin('p.category', 'c')
            ->where('c = :category')
            ->andWhere('p.status = :status')
            ->setParameters([
                'category' => $category,
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'DESC');

        /** @var SlidingPagination $pagination */
        $pagination = $this
            ->paginator
            ->paginate(
                $qb->getQuery(),
                $page,
                $itemsPerPage ?? $this->itemsPerPage
            );

        return $pagination;
    }

    /**
     * @param Tag $tag
     * @param int $page
     * @param int|null $itemsPerPage
     * @return SlidingPagination
     */
    public function getByTag(Tag $tag, $page, $itemsPerPage = null)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p, m')
            ->leftJoin('p.cover', 'm')
            ->innerJoin('p.tags', 't')
            ->where('t = :tag')
            ->andWhere('p.status = :status')
            ->setParameters([
                'tag' => $tag,
                'status' => Post::STATUS_PUBLISHED,
            ])
            ->orderBy('p.published', 'DESC');

        /** @var SlidingPagination $pagination */
        $pagination = $this->paginator->paginate(
            $qb->getQuery(),
            $page,
            $itemsPerPage ?? $this->itemsPerPage
        );

        return $pagination;
    }

    /**
     * @return array
     */
    public function getCalendar()
    {
        $connection = $this->getEntityManager()->getConnection();
        $sql = "SELECT Date_format(published, '%m') as month,
                        Date_format(published, '%Y') as year,
                        COUNT(*) AS count
                 FROM   blog__post
                 WHERE status = 1
                 GROUP  BY YEAR(published),
                           MONTH(published)
                 ORDER  BY published DESC";

        return $connection->query($sql)->fetchAll();
    }

    public function publishPosts()
    {
        $em = $this->getEntityManager();
        $q = $em->createQuery("UPDATE Xaben\\BlogBundle\\Entity\\Post p 
            SET p.status = :newStatus, p.published = :currentDate
            WHERE p.publishDate IS NOT NULL and
             p.publishDate < :currentDate AND 
             p.status = :oldStatus
        ");

        return $q->execute([
            'oldStatus' => Post::STATUS_SCHEDULED,
            'newStatus' => Post::STATUS_PUBLISHED,
            'currentDate' => new \DateTime(),
        ]);
    }
}
