<?php

namespace Xaben\BlogBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class TagRepository extends EntityRepository
{
    /**
     * @param string $letters
     * @return array
     */
    public function getListTags($letters)
    {
        $tags = $this
            ->createQueryBuilder('t')
            ->select('t.title')
            ->where('t.title LIKE :search')
            ->setParameter('search', '%'.$letters.'%')
            ->getQuery()
            ->getArrayResult();

        $newTags = [];
        foreach ($tags as $key => $tag) {
            $newTags[] = ['text' => $tag['title']];
        }

        return $newTags;
    }
}
