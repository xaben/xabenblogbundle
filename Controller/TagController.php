<?php

namespace Xaben\BlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    /**
     * @Route("/tags/get", name="xaben_blog_ajax_tags")
     *
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $letters = $request->query->get('q', '');

        if (strlen($letters) < 2) {
            return new JsonResponse([]);
        }

        $tags = $this
            ->get('xaben.blog.repository.tag')
            ->getListTags($letters);

        return new JsonResponse($tags);
    }
}
