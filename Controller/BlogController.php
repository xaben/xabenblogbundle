<?php

namespace Xaben\BlogBundle\Controller;

use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Xaben\BlogBundle\Entity\Category;
use Xaben\BlogBundle\Entity\Post;
use Xaben\BlogBundle\Entity\Tag;

class BlogController extends Controller
{
    /**
     * @Route(
     *     "/{page}",
     *     name="xaben_blog_homepage",
     *     defaults={ "page": 1 },
     *     requirements={ "page": "\d+" },
     *     options={ "expose"="true" }
     *     )
     *
     * @param int $page
     * @return Response
     */
    public function listLatestAction(Request $request, $page)
    {
        $posts = $this
            ->get('xaben.blog.repository.post')
            ->getLatest(
                $page,
                $request->get('items')
            );

        return $this->generateView($request, 'XabenBlogBundle:Blog:index.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route(
     *     "/{year}/{month}/{page}",
     *     name="xaben_blog_date",
     *     defaults={ "page": 1 },
     *     requirements={ "page": "\d+", "year": "\d+", "month": "\d+" },
     *     options={ "expose"="true" }
     *     )
     *
     * @param int $year
     * @param int $month
     * @param int $page
     * @return Response
     */
    public function listByPeriodAction(Request $request, $year, $month, $page)
    {
        $posts = $this
            ->get('xaben.blog.repository.post')
            ->getByMonth($year, $month, $page, $request->get('items'));

        return $this->generateView($request,'XabenBlogBundle:Blog:index.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route(
     *     "/category/{slug}-{id}/{page}",
     *     name="xaben_blog_category",
     *     defaults={ "page": 1 },
     *     requirements={ "page": "\d+", "slug": "[a-zA-Z0-9-]+", "id": "\d+" },
     *     options={ "expose"="true" }
     *     )
     *
     * @param Category $category
     * @param int $page
     * @return Response
     */
    public function listByCategoryAction(Request $request, Category $category, $page)
    {
        $posts = $this
            ->get('xaben.blog.repository.post')
            ->getByCategory($category, $page, $request->get('items'));

        return $this->generateView(
            $request,
            'XabenBlogBundle:Blog:category.html.twig',
            [
                'posts' => $posts,
                'category' => $category,
            ]
        );
    }

    /**
     * @Route(
     *     "/tag/{slug}-{id}/{page}",
     *     name="xaben_blog_tag",
     *     defaults={ "page": 1 },
     *     requirements={ "page": "\d+", "slug": "[a-zA-Z0-9-]+", "id": "\d+" },
     *     options={ "expose"="true" }
     *     )
     *
     * @param Tag $tag
     * @param int $page
     * @return Response
     */
    public function listByTagAction(Request $request, Tag $tag, $page)
    {
        $posts = $this
            ->get('xaben.blog.repository.post')
            ->getByTag($tag, $page, $request->get('items'));

        return $this->generateView(
            $request,
            'XabenBlogBundle:Blog:tag.html.twig',
            [
                'posts' => $posts,
                'tag' => $tag
            ]
        );
    }

    /**
     * @Route(
     *     "/{year}/{month}/{slug}-{id}",
     *     name="xaben_blog_post",
     *     requirements={ "year": "\d+", "month": "\d+", "slug": "[a-zA-Z0-9-]+", "id": "\d+" },
     *     options={ "expose"="true" }
     *     )
     *
     * @param Post $post
     * @return Response
     */
    public function postAction(Request $request, Post $post)
    {
        $postRepository = $this->get('xaben.blog.repository.post');
        $nextPost = $postRepository->getNextPost($post);
        $previousPost = $postRepository->getPreviousPost($post);

        $session = $this->get("session");
        if ($session->has('blogposts')) {
            $postIds = $session->get('blogposts');
        } else {
            $postIds = array();
        }

        if (!in_array($post->getId(), $postIds)) {
            $postIds[] = $post->getId();
            $session->set('blogposts', $postIds);

            $post->setViews($post->getViews() + 1);
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($post);
            $em->flush();
        }

        return $this->generateView(
            $request,
            'XabenBlogBundle:Blog:post.html.twig',
            [
                'post' => $post,
                'nextPost' => $nextPost,
                'previousPost' => $previousPost,
            ],
            ['details']
        );
    }

    public function generateView(
        Request $request,
        $view,
        array $parameters = [],
        array $serializationContexts = []
    ) {
        if ($request->headers->get('Accept') === 'application/json') {
            $serializer = $this->container->get('jms_serializer');
            $data = $serializer->serialize($parameters, 'json', SerializationContext::create()->setGroups(
                array_merge(
                    ['Default'],
                    $serializationContexts
                )
            ));

            $response = new JsonResponse($data, 200, [], true);
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;
        }

        return $this->render($view, $parameters);
    }
}
