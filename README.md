## Xaben Blog Bundle

# Installation
 - Require Bundle
  ```
    composer require xaben/blog-bundle
  ```

 - Add to AppKernel the following:
 ```
    new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
    new Xaben\BlogBundle\XabenBlogBundle(),
    new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
    
 ```
 
 - Update schema run:
 ```
    php app/console doctrine:schema:update --force
 ```
 
 - Enable Twig extension add the following to services.yml
 ```
 services:
     twig.extension.intl:
         class: Twig_Extensions_Extension_Intl
         tags:
             - { name: twig.extension }
 ```
 
 - Enable Sluggable behavior, add to AppKernel:
 ```yml
             new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
 ```

- Add CKEditor configuration
```yml
ivory_ck_editor:
    default_config: blog
    plugins:
        wpmore:
            path:     "/bundles/xabenblog/wpmore/"
            filename: "plugin.js"
    configs:
        blog:
            allowedContent: true
            extraPlugins: "wpmore"
            toolbar:  [ [ 'Format', 'Bold', 'Italic', 'Underline', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-', 'Image', 'Link', 'Unlink', 'Table'],  [ 'Maximize', 'DisplayBlocks', 'Source' ], [ 'WPMore' ] ]
            height: 500px
            enterMode: CKEDITOR.ENTER_BR
            shiftEnterMode: CKEDITOR.ENTER_P

            filebrowserBrowseRoute: admin_app_media_browser
            filebrowserImageBrowseRoute: admin_app_media_browser
            # Display images by default when clicking the image dialog browse button
            filebrowserImageBrowseRouteParameters:
                provider: sonata.media.provider.image
            filebrowserUploadRoute: admin_app_media_upload
            filebrowserUploadRouteParameters:
                provider: sonata.media.provider.file
            # Upload file as image when sending a file from the image dialog
            filebrowserImageUploadRoute: admin_app_media_upload
            filebrowserImageUploadRouteParameters:
                provider: sonata.media.provider.image
                context: blog # Optional, to upload in a custom context

```

- Add form field configuration:
```yml
# Twig Configuration
twig:
    ...
    form_themes:
        - 'XabenBlogBundle:Form:fields.html.twig'
```

- Add Context "Blog" to Sonata Media Bundle
```yml
sonata_media:
    contexts:
        blog:
            download:
                strategy: sonata.media.security.public_strategy
                mode: http
            providers:
                - sonata.media.provider.image

            formats:
                default: { width: 800 , quality: 80}
```


- Configure Sonata Media Entity to use (default is: `Application\Sonata\MediaBundle\Entity\Media`), add in `config.yml`:

```yml
parameters:
    xaben.blog.sonata_media_class: 'AppBundle\Entity\Media'
```
