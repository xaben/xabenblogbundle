<?php
namespace Xaben\BlogBundle\EventListener;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Xaben\BlogBundle\Entity\Post;

/**
 * Add data after serialization
 */
class SerializationListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /** @var string */
    private $mediaClass;

    /** @var string */
    private $baseUrl;

    /**
     * @param ContainerInterface $container
     * @param string $mediaClass
     * @param string $baseUrl
     */
    public function __construct(ContainerInterface $container, $mediaClass, $baseUrl)
    {
        $this->container = $container;
        $this->mediaClass = $mediaClass;
        $this->baseUrl = $baseUrl;
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $entity = $event->getObject();
        if ($entity instanceof $this->mediaClass) {
            $provider = $this->container->get($entity->getProviderName());
            $event->getVisitor()->addData(
                'url',
                $this->baseUrl . $provider->generatePublicUrl($entity, $provider->getFormatName($entity, 'default'))
            );
            $event->getVisitor()->addData(
                'reference_url',
                $this->baseUrl . $provider->generatePublicUrl($entity, $provider->getFormatName($entity, 'reference'))
            );
        }

        if ($entity instanceof Post) {
            $publishedDate = $entity->getPublished() ?: new \DateTime();
            $router = $this->container->get('router');
            $event->getVisitor()->setData(
                'description',
                str_replace('src="/', 'src="'. $this->baseUrl . '/', $entity->getContent())
            );
            $event->getVisitor()->setData(
                'content',
                str_replace('src="/', 'src="'. $this->baseUrl . '/', $entity->getContent())
            );
            $event->getVisitor()->addData(
                'url',
                $this->baseUrl . $router->generate('xaben_blog_post', [
                    'id' => $entity->getId(),
                    'slug' => $entity->getSlug(),
                    'month' => $publishedDate->format('m'),
                    'year' => $publishedDate->format('Y'),
                ])
            );
        }
    }
}
