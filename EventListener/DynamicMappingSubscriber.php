<?php

namespace Xaben\BlogBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class DynamicMappingSubscriber implements EventSubscriber
{
    /**
     * @var string
     */
    private $sonataMediaEntity;

    /**
     * DynamicMappingSubscriber constructor.
     */
    public function __construct($sonataMediaEntity)
    {
        $this->sonataMediaEntity = $sonataMediaEntity;
    }


    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata,
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is the whole mapping info for this class
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'Xaben\BlogBundle\Entity\Post') {
            return;
        }

        $metadata->mapOneToOne(array(
            'targetEntity'  => $this->sonataMediaEntity,
            'fieldName'     => 'cover',
            'cascade'       => array('all'),
            'orphanRemoval' => true,
            'fetch'         => 'EAGER'
        ));
    }
}
