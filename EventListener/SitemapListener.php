<?php
namespace Xaben\BlogBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Xaben\BlogBundle\Entity\Post;

/**
 * @author Alexandru Benzari <benzari.alex@gmail.com>
 */
class SitemapListener implements SitemapListenerInterface
{
    private $router;
    private $em;

    public function __construct(RouterInterface $router, EntityManager $em)
    {
        $this->router = $router;
        $this->em = $em;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $section = $event->getSection();
        if (is_null($section) || $section == 'blog') {

            //get list of all pages
            $posts = $this->em->getRepository('XabenBlogBundle:Post')->findAll();

            foreach ($posts as $post) {
                /** @var Post $post */

                if ($post->getPublished() && $post->getStatus() == 1) {
                    //get absolute url
                    $url = $this->router->generate(
                        'xaben_blog_post',
                        [
                            'year' => $post->getPublished()->format('Y'),
                            'month' => $post->getPublished()->format('m'),
                            'slug' => $post->getSlug(),
                            'id' => $post->getId(),
                        ],
                        true);

                    //add homepage url to the urlset named default
                    $event->getGenerator()->addUrl(
                        new UrlConcrete(
                            $url,
                            $post->getModified(),
                            UrlConcrete::CHANGEFREQ_WEEKLY,
                            1
                        ),
                        'blog'
                    );
                }
            }
        }
    }
}
